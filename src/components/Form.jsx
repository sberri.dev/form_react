import { useEffect, useState } from "react";

function BasicForm() {
  // on prépare une valeur de state 'value' avec useState.
  // setValue() sera appelé à chaque changement de valeur de l'input
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [age, setAge] = useState("");
  const [email, setEmail] = useState("");
  const [passWord, setPassWord] = useState("");

  // La fonction handleSubmit() sera appelée lors de la validation du formulaire
  const handleSubmit = (event) => {
    event.preventDefault();
  };

  // function validate(input) {
  //   return input > 18 ? true: false;
  // }

  const [errorMessage, setErrorMessage] = useState('');

useEffect(() => {
  if (age < 18) {
    setErrorMessage('You must have 18 years old coquinou');
  } else {
    setErrorMessage('');
  }
}, [age]);


  // Et voilà le formulaire
  return (
    <form onSubmit={handleSubmit}>
      <div>
        First name :{" "}
        <input
          type="text"
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
        />
      </div>
      <div>
        Last name :{" "}
        <input
          type="text"
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
        />
      </div>
      <div>
        Age :{" "}
        <input
          type="number"
          value={age}
          onChange={(e) => setAge(e.target.value)} />
          {errorMessage && <div>{errorMessage}</div>}
       
      </div>
      <div>
        Email address :{" "}
        <input
          type="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
      </div>
      <div>
        Pass word :{" "}
        <input
          type="password"
          value={passWord}
          onChange={(e) => setPassWord(e.target.value)}
        />
      </div>
      <button type="submit">Suscribe</button>
    </form>
  );
}

export default BasicForm;
